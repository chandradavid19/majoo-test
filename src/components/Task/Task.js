import { React, Component } from 'react';
import Card from '../Card/Card';
import AddCard from '../Card/AddCard';

class Task extends Component {
	render() {
		const {
			type,
			handleClickCard,
			handleClickMoveCard,
			handleClickDeleteCard,
			section,
			isCreateCard,
			createCardText,
			onEnterPress,
			handleChangeCardText,
			handleCreateCard,
			code,
		} = this.props;
		return (
			<>
				<div className='majoo__titleContainer'>
					<span className='majoo__titleText'>{code === 'todo' ? 'To Do' : 'Done'}</span>
				</div>
				{type.length > 0 &&
					type.map((task, index) => {
						return (
							<div key={index}>
								<Card
									task={task}
									handleClickCard={() => handleClickCard(task, code)}
									handleClickMoveCard={() => handleClickMoveCard(task)}
									handleClickDeleteCard={() => handleClickDeleteCard(task)}
									code={code}
								/>
							</div>
						);
					})}
				{isCreateCard && code === section ? (
					<AddCard
						createCardText={createCardText}
						onEnterPress={(e) => onEnterPress(e)}
						handleChangeCardText={(value) => handleChangeCardText(value)}
					/>
				) : (
					<div className='majoo__addCard' onClick={() => handleCreateCard(code)}>
						<span>+ Add a Card</span>
					</div>
				)}
			</>
		);
	}
}

export default Task;
