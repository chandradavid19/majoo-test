import { React, Component } from 'react';
import './Modal.css';
import Overlay from '../Overlay/Overlay';

class Modal extends Component {
	render() {
		const {
			isModalActive,
			task,
			handleClickOverlay,
			handleChangeTitle,
			handleChangeDescription,
			handleClickSave,
			handleClickCancel,
			handleClickDelete,
			isEditTitle,
			onEnterPress,
			handleClickTitle,
		} = this.props;
		// console.log(task);
		if (isModalActive) {
			return (
				<>
					<Overlay isOverlayActive={isModalActive} onClick={handleClickOverlay} />
					<div className='majoo__modalContainer'>
						<div>
							<div className='majoo__dataContainer'>
								<div onClick={handleClickCancel} className='majoo__closeIconContainer'>
									<img src={require('../../assets/close.png')} className='majoo__closeIconImg majoo__pointer' />
								</div>
								<div onClick={handleClickTitle} className='majoo__titleWrap'>
									{isEditTitle ? (
										<textarea
											name='titleText'
											placeholder='Enter a title for this card..'
											value={task.title}
											onChange={(e) => handleChangeTitle(e)}
											onKeyDown={(e) => onEnterPress(e)}
											className='majoo__addCardTitleTextArea'
										></textarea>
									) : (
										<span className='majoo__titleText'>{task.title}</span>
									)}
								</div>
								<span className='majoo__descriptionTitle'>Description</span>
								<textarea
									name='descriptionText'
									placeholder='Add a more detailed description..'
									value={task.description}
									onChange={(e) => handleChangeDescription(e)}
									className='majoo__addCardDescriptionTextArea'
								></textarea>
								<div className='majoo__helpButton'>
									<div className='majoo__deleteButton majoo__pointer' onClick={handleClickDelete}>
										<span>Delete</span>
									</div>
									<div className='majoo__submitButton majoo__pointer' onClick={handleClickSave}>
										<span>Save</span>
									</div>
								</div>
							</div>
							<div className='majoo__sidebar'></div>
						</div>
					</div>
				</>
			);
		} else {
			return null;
		}
	}
}

export default Modal;
