import { React, Component } from 'react';
import './Overlay.css';

class Overlay extends Component {
	render() {
		const { onClick, isOverlayActive } = this.props;
		return <div onClick={onClick} className={`majoo__overlay ${isOverlayActive ? 'majoo__block' : 'majoo__hidden'}`}></div>;
	}
}

export default Overlay;
