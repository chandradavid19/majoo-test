import { React, Component } from 'react';
import './Card.css';

class Card extends Component {
	render() {
		const { task, handleClickCard, handleClickMoveCard, handleClickDeleteCard, code } = this.props;
		return (
			<div className='majoo__taskCardContainer'>
				<div className='majoo__taskCard' onClick={handleClickCard}>
					<span className='majoo__wordBreak'>{task.title}</span>
				</div>
				<div className='majoo__ml10'>
					<div onClick={handleClickMoveCard} className='majoo__mb10'>
						<img src={require('../../assets/right-arrow.png')} className='majoo__iconImg majoo__pointer' />
					</div>
					{code === 'todo' && (
						<div onClick={handleClickDeleteCard}>
							<img src={require('../../assets/trash-can.png')} className='majoo__iconImg majoo__pointer' />
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default Card;
