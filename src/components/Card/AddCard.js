import { React, Component } from 'react';

class AddCard extends Component {
	render() {
		const { createCardText, onEnterPress, handleChangeCardText } = this.props;
		return (
			<form ref={(el) => (this.myFormRef = el)}>
				<textarea
					name='createCardText'
					placeholder='Enter a title for this card..'
					value={createCardText}
					onChange={(e) => handleChangeCardText(e.target.value)}
					onKeyDown={(e) => onEnterPress(e)}
					className={'majoo__addCardTextArea'}
				></textarea>
			</form>
		);
	}
}

export default AddCard;
