import { React, Component } from 'react';
import './App.css';
import axios from 'axios';
import moment from 'moment';
import Modal from './components/Modal/Modal';
import Task from './components/Task/Task';

moment.locale('id');

class App extends Component {
	constructor() {
		super();
		this.state = {
			todo: [],
			done: [],
			isCreateCard: false,
			createCardText: '',
			section: '',
			isModalActive: false,
			modalData: {},
			isEditTitle: false,
		};
	}
	componentDidMount() {
		this.getTaskData();
	}
	getTaskData = () => {
		let todo = [];
		let done = [];
		axios.get(`https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list`).then((res) => {
			if (res.status === 200) {
				const data = res.data;
				for (let i = 0; i < data.length; i++) {
					if (data[i].status === 0) {
						todo.push(data[i]);
					} else {
						done.push(data[i]);
					}

					if (i === data.length - 1) {
						const sortedTodo = todo.sort(function (a, b) {
							return new Date(a.createdAt) - new Date(b.createdAt);
						});
						const sortedDone = done.sort(function (a, b) {
							return new Date(b.createdAt) - new Date(a.createdAt);
						});
						this.setState({
							todo: sortedTodo,
							done: sortedDone,
						});
					}
				}
			}
		});
	};
	onEnterPress = (e) => {
		if (e.keyCode == 13 && e.shiftKey == false) {
			e.preventDefault();
			const newData = {
				createdAt: moment(new Date()).format('YYYY-MM-DD HH:MM'),
				description: '',
				id: Math.floor(Math.random() * 100),
				status: this.state.section === 'todo' ? 0 : 1,
				title: this.state.createCardText,
			};
			const value = this.state.section === 'todo' ? this.state.todo : this.state.done;
			value.push(newData);
			this.setState({
				createCardText: '',
				section: '',
				isCreateCard: false,
			});
		}
	};

	handleClickCard = (task, section) => {
		this.setState({
			isModalActive: true,
			modalData: task,
			section: section,
		});
	};

	handleClickOverlay = () => {
		this.setState({
			isModalActive: false,
			isEditTitle: false,
			modalData: {},
			section: '',
		});
	};

	handleChangeTitle = (e) => {
		this.setState((prevState) => ({
			modalData: {
				...prevState.modalData,
				title: e.target.value,
			},
		}));
	};

	handleChangeDescription = (e) => {
		this.setState((prevState) => ({
			modalData: {
				...prevState.modalData,
				description: e.target.value,
			},
		}));
	};

	handleClickCancel = () => {
		const { section, todo, done, modalData } = this.state;
		const value = section === 'todo' ? todo : done;
		const originalData = value.filter((task) => task.id === modalData.id)[0];

		this.setState(
			(prevState) => ({
				isEditTitle: false,
				modalData: {
					...prevState.modalData,
					title: originalData.title,
					description: originalData.description,
				},
			}),
			() => this.handleClickOverlay()
		);
	};

	handleClickSave = () => {
		const { section, todo, done, modalData } = this.state;
		const value = section === 'todo' ? todo : done;
		const objIndex = value.findIndex((obj) => obj.id == modalData.id);

		value[objIndex].title = modalData.title;
		value[objIndex].description = modalData.description;
		this.handleClickOverlay();
	};

	modalTitleOnEnterPress = (e) => {
		if (e.keyCode == 13 && e.shiftKey == false) {
			const { section, todo, done, modalData } = this.state;
			const value = section === 'todo' ? todo : done;
			const objIndex = value.findIndex((obj) => obj.id == modalData.id);

			value[objIndex].title = modalData.title;
			this.setState({ isEditTitle: false });
		}
	};

	handleClickTitle = () => {
		this.setState({ isEditTitle: true });
	};
	handleClickDeleteCard = (task) => {
		if (task) {
			this.setState(
				{
					modalData: task,
					section: task.status === 0 ? 'todo' : 'done',
				},
				() => this.handleClickDelete()
			);
		}
	};
	handleClickDelete = (move) => {
		const { section, todo, done, modalData } = this.state;
		const value = section === 'todo' ? todo : done;
		const objIndex = value.findIndex((obj) => obj.id == modalData.id);

		if (move !== undefined) {
			value.splice(objIndex, 1);
		} else {
			if (value !== done) {
				value.splice(objIndex, 1);
				this.setState(
					{
						isEditTitle: false,
					},
					() => this.handleClickOverlay()
				);
			} else {
				alert('Done Task cant be Delete');
			}
		}
	};

	handleClickMoveCard = (task) => {
		if (task) {
			this.setState(
				{
					modalData: task,
					section: task.status === 0 ? 'todo' : 'done',
				},
				() => {
					this.handleClickDelete('move');
					this.handleClickMove();
				}
			);
		}
	};

	handleClickMove = () => {
		const { section, todo, done, modalData } = this.state;
		const value = section === 'todo' ? done : todo; // we reverse this because we want to add, not remove.
		const updatedData = {
			...modalData,
			status: section === 'todo' ? 1 : 0,
		};
		value.push(updatedData);

		if (value === done) {
			const sortedDone = value.sort(function (a, b) {
				return new Date(b.createdAt) - new Date(a.createdAt);
			});
			this.setState({ done: sortedDone, section: '', modalData: {} });
		} else {
			const sortedTodo = todo.sort(function (a, b) {
				return new Date(a.createdAt) - new Date(b.createdAt);
			});
			this.setState({ todo: sortedTodo, section: '', modalData: {} });
		}
	};

	handleChangeCardText = (value) => {
		this.setState({ createCardText: value });
	};

	handleCreateCard = (value) => {
		this.setState({ isCreateCard: true, section: value });
	};
	render() {
		const { todo, done, createCardText, isCreateCard, section } = this.state;
		return (
			<div className='majoo__container'>
				<Modal
					isModalActive={this.state.isModalActive}
					task={this.state.modalData}
					isEditTitle={this.state.isEditTitle}
					handleClickOverlay={() => this.handleClickOverlay()}
					handleChangeDescription={(value) => this.handleChangeDescription(value)}
					handleChangeTitle={(value) => this.handleChangeTitle(value)}
					handleClickSave={() => this.handleClickSave()}
					handleClickCancel={() => this.handleClickCancel()}
					onEnterPress={(e) => this.modalTitleOnEnterPress(e)}
					handleClickTitle={() => this.handleClickTitle()}
					handleClickDelete={() => this.handleClickDelete()}
				/>
				<div className='majoo__listContainer'>
					<Task
						type={todo}
						handleClickCard={(task, section) => this.handleClickCard(task, section)}
						handleClickMoveCard={(task) => this.handleClickMoveCard(task)}
						handleClickDeleteCard={(task) => this.handleClickDeleteCard(task)}
						code='todo'
						section={section}
						isCreateCard={isCreateCard}
						onEnterPress={(e) => this.onEnterPress(e)}
						handleChangeCardText={(value) => this.handleChangeCardText(value)}
						handleCreateCard={(value) => this.handleCreateCard(value)}
						createCardText={createCardText}
					/>
				</div>
				<div className='majoo__listContainer'>
					<Task
						type={done}
						handleClickCard={(task, section) => this.handleClickCard(task, section)}
						handleClickMoveCard={(task) => this.handleClickMoveCard(task)}
						code='done'
						section={section}
						isCreateCard={isCreateCard}
						onEnterPress={(e) => this.onEnterPress(e)}
						handleChangeCardText={(value) => this.handleChangeCardText(value)}
						handleCreateCard={(value) => this.handleCreateCard(value)}
						createCardText={createCardText}
					/>
				</div>
			</div>
		);
	}
}

export default App;
